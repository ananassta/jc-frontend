const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const MomentLocalesPlugin = require('moment-locales-webpack-plugin')

const cssRules = modules => [
    {
        loader: MiniCssExtractPlugin.loader,
        options: {
            hmr: process.env.NODE_ENV === 'development',
        },
    },
    {
        loader: 'css-loader',
        options: {
            modules,
        },
    },
    {
        loader: 'postcss-loader',
        options: {
            ident: 'postcss',
            plugins: [require('autoprefixer')()],
        },
    },
]

const lessRule = modules => ({
    test: /\.less$/,
    use: [
        ...cssRules(modules),
        {
            loader: 'less-loader',
            options: {
                javascriptEnabled: true,
            },
        },
    ],
})

module.exports = () => ({
    entry: './src/index.tsx',

    module: {
        rules: [
            {
                ...lessRule(true),
                exclude: /node_modules/,
            },
            {
                ...lessRule(false),
                include: /node_modules/,
            },
            {
                test: /\.tsx?$/,
                use: ['babel-loader'],
                exclude: /node_modules/,
            },
            {
                test: /\.css$/i,
                use: [...cssRules(false), 'css-loader'],
            },
        ],
    },

    resolve: {
        extensions: ['.wasm', '.tsx', '.ts', '.js', '.jsx', '.json'],
    },

    mode: 'development',
    devtool: 'eval',

    devServer: {
        contentBase: './dist',
        port: 1234,
        hot: true,
        historyApiFallback: true,
    },

    output: {
        filename: 'jc.js',
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: 'Юридическая клиника',
        }),
        new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css',
            chunkFilename: '[name].[contenthash].css',
        }),
        new MomentLocalesPlugin({
            localesToKeep: ['ru'],
        }),
    ],
})
