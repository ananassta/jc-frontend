import React, { lazy } from 'react'
import { useRoute } from 'react-router5'
import { RouteContext } from 'react-router5/types/types'
import createRouter from 'router5'
import browserPlugin from 'router5-plugin-browser'

export const routes = [
    {
        name: 'me',
        path: '/',
    },
    {
        name: 'students',
        path: '/st',
    },
] as const

type RouteName = (typeof routes)[number]['name']

export const routeInfo: {
    [K in RouteName]: {
        icon: string
        view: React.ComponentType
        title: string
    }
} = {
    me: {
        icon: 'idcard',
        view: lazy(() => import(/* webpackChunkName: 'PageMe' */ '../PageMe/PageMe')),
        title: 'Я',
    },
    students: {
        icon: 'team',
        view: lazy(() =>
            import(/* webpackChunkName: 'PageStudents' */ '../PageStudents/PageStudents')
        ),
        title: 'Студенты',
    },
}
const defaultRoute: RouteName = 'me'

export const router = createRouter(routes as any, { defaultRoute })
router.usePlugin(browserPlugin())
router.start()

export const useJCRoute = () => {
    return useRoute() as RouteContext & { route: { name: RouteName } }
}
