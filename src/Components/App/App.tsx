import React from 'react'
import { RouterProvider } from 'react-router5'

import { ErrorBoundary } from '../ErrorBoundary'
import { JCLayout } from './Layout'
import { router } from './router'

export const App = () => {
    return (
        <ErrorBoundary seed={''}>
            <RouterProvider router={router}>
                <JCLayout />
            </RouterProvider>
        </ErrorBoundary>
    )
}
