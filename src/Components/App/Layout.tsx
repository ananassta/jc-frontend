import { Icon, Layout, Menu } from 'antd'
import React, { Suspense, useState } from 'react'
import { Link } from 'react-router5'

import { ErrorBoundary } from '../ErrorBoundary'
const { Content, Sider } = Layout

import styles from './Layout.less'
import { routeInfo, routes, useJCRoute } from './router'

export const JCLayout = () => {
    const [collapsed, setCollapsed] = useState(false)

    const r5 = useJCRoute()
    const View = routeInfo[r5.route.name].view

    return (
        <Layout style={{ minHeight: '100vh' }}>
            <Sider
                collapsible={true}
                collapsed={collapsed}
                onCollapse={setCollapsed}
                theme={'light'}
            >
                <Menu mode="inline" selectedKeys={[r5.route.name]} className={styles.menu}>
                    {routes.map(route => (
                        <Menu.Item key={route.name}>
                            <Link routeName={route.name} key={route.name}>
                                <Icon type={routeInfo[route.name].icon} />
                                <span>{routeInfo[route.name].title}</span>
                            </Link>
                        </Menu.Item>
                    ))}
                </Menu>
            </Sider>
            <Content>
                <ErrorBoundary seed={r5.route.name}>
                    <Suspense fallback={null}>
                        <View />
                    </Suspense>
                </ErrorBoundary>
            </Content>
        </Layout>
    )
}
