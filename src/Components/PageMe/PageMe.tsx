import { Card } from 'antd'
import React from 'react'

import { Page } from '../Page/Page'

const PageMe = () => {
    return (
        <Page>
            <Card>Awsome Me</Card>
        </Page>
    )
}

export default PageMe
