import { Card } from 'antd'
import React from 'react'

import { Page } from '../Page/Page'

const PageStudents = () => {
    return (
        <Page>
            <Card>Students</Card>
        </Page>
    )
}

export default PageStudents
